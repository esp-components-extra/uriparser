//SPDX-License-Identifier: BSD-2-Clause-Patent
//
//Copyright (c) 2020 Albert Krenz <albert.krenz@mailbox.org>

#include <uriparser/Uri.h>

#include <stdio.h>

void app_main(){
    char toUnescape[] = "one%20two";

    printf("original:  \"%s\"\n", toUnescape);
    uriUnescapeInPlaceA(toUnescape);
    printf("unescaped: \"%s\"\n", toUnescape);
}

