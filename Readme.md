<!-- SPDX-License-Identifier: CC-BY-4.0-->
<!-- This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA. -->

<!-- Copyright (c) 2020 Albert Krenz <albert.krenz@mailbox.org> -->

# ESP Uriparser Wrapper
This is a Wrapper for uriparser (https://uriparser.github.io/) to be used as Component in Espressif IDF (https://github.com/espressif/esp-idf)

# How to uriparser include into your project
To include this component into your project create a `components` directory under your project folder

    <your-project-path>
    ├── CMakeLists.txt
    ├── components
    │   └── uriparser
    ├── main
    │   ├── CMakeLists.txt
    │   └── main.c


Add the wrapper as submodule to git via

    # git submodule add components/uriparser git@gitlab.com:esp-components-extra/uriparser.git
    # git submodule init && git submodule update --resursive

Or clone this repository directly into the components folder

    # cd components
    # git clone --recursive git@gitlab.com:esp-components-extra/uriparser.git


# How to use
Example use could look as following

```c
#include <uriparser/Uri.h>

#include <stdio.h>

void app_main(){
  char toUnescape[] = "one%20two";

  printf("original:  \"%s\"\n", toUnescape);
  uriUnescapeInPlaceA(toUnescape);
  printf("unescaped: \"%s\"\n", toUnescape);
}
```
You can find a full example under the example directory. Just go there and run a build via `idf.py build`.

For more information/examples on how to use uriparser see the documentation at: https://uriparser.github.io/doc/api/latest/

# License
This Wrapper is licensed under BSD-2-Clause Plus Patent License. For the full License text see the LICENSE file